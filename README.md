[![pipeline status](https://gitlab.com/xwildeyes/dart-telnet/badges/master/pipeline.svg)](https://gitlab.com/xwildeyes/dart-telnet/commits/master)

# Telnet for Dart

Simple telnet client for Dart in WIP quality, passing 2/6 tests and being ported from [mkozjak/node-telnet-client](https://github.com/mkozjak/node-telnet-client).

I wrote this over a weekend to better learn telnet and Dart, please report any issue!

## Tests

To run the tests, `cd test && npm install`.