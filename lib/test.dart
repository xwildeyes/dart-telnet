import 'dart:async';
import 'dart:convert';
import 'dart:io';
import 'package:colorize/colorize.dart';

var host = '127.0.0.1';
var port = 2323;

Future<Process> setupNodeTelnetServer(String testId) {
  var completer = Completer<Process>();

  Process.start(
    'node',
    ['test/telnet-server.js', testId],
  ).then((process) {
    // print debug information from node-telnet-server in different color.
    process.stderr
        .transform(utf8.decoder)
        .listen((data) => color(data, front: Styles.LIGHT_MAGENTA));

    process.stdout.transform(utf8.decoder).listen((data) {
      if (data.contains('Telnet server listening on 2323'))
        completer.complete(process);
      color(data, front: Styles.RED);
    });
  });
  return completer.future;
}
