import 'dart:convert';
import 'dart:typed_data';

class ConnectEvent {}

class DataEvent {
  String data;

  DataEvent(Uint8List _data) {
    data = utf8.decode(_data);
  }
}

class ErrorEvent {
  dynamic error;

  ErrorEvent(this.error);
}

class CloseEvent {}

class FailedLoginEvent {}

class ReadyEvent {
  String shellPrompt;

  ReadyEvent(this.shellPrompt);
}

class BufferexceededEvent {}

class ResponsereadyEvent {}

class WritedoneEvent {}
