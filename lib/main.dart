import 'dart:async';
import 'dart:io';
import 'dart:typed_data';

import 'package:event_bus/event_bus.dart';
import 'package:telnet/common.dart';
import 'package:telnet/events.dart';

class Telnet extends EventBus {
  Socket _socket = null;
  String _state = null;
  bool loginPromptReceived = false;
  String _inputBuffer = '';
  bool _isSocketClosed = null;
  List<String> _response = null;
  bool promptSameLine = false;

  /// Host the client should connect to. Defaults to '127.0.0.1'.
  String host;

  /// Port the client should connect to. Defaults to '23'.
  int port;

  /// Local interface to bind for network connections. Defaults to an empty string. More information can be found here.
  String localAddress;

  /// Allows to pass an object, which can contain every property from Node's SocketConnectOpts. Defaults to an empty object. Properties defined inside this object will overwrite any of the three above properties. More information can be found here.
  // String socketConnectOptions;
  /// Sets the socket to timeout after the specified number of milliseconds. of inactivity on the socket.
  // String timeout;
  /// Shell prompt that the host is using. Can be a string or an instance of RegExp. Defaults to regex '/(?:/ )?#\s/'.
  dynamic shellPrompt = new RegExp('(?:/ )?#\s');

  /// Username/login prompt that the host is using. Can be a string or an instance of RegExp. Defaults to regex '/login[: ]*$/i'.
  String loginPrompt;

  /// Password/login prompt that the host is using. Can be a string or an instance of RegExp. Defaults to regex '/Password: /i'.
  String passwordPrompt;

  /// String or regex to match if your host provides login failure messages. Defaults to null.
  String failedLoginMatch = null;

  /// Flag used to determine if an initial '\r\n' (CR+LF) should be sent when connected to server.
  bool initialLFCR;

  /// Username used to login. Defaults to 'root'.
  String username;

  /// Password used to login. Defaults to ''.
  String password;

  /// Duplex stream which can be used for connection hopping/reusing.
  // dynamic sock;
  /// Input record separator. A separator used to distinguish between lines of the response. Defaults to '\r\n'.
  String irs;

  /// Output record separator. A separator used to execute commands (break lines on input). Defaults to '\n'.
  String ors;

  /// The number of lines used to cut off the response. Defaults to 1.
  int echoLines;

  /// Whether shell prompt should be excluded from the results. Defaults to true.
  bool stripShellPrompt;

  /// The pattern used (and removed from final output) for breaking the number of lines on output. Defaults to '---- More'.
  String pageSeparator;

  /// Disable telnet negotiations if needed. Can be used with 'send' when telnet specification is not needed. Telnet client will then basically act like a simple TCP client. Defaults to true.
  bool negotiationMandatory;

  /// A timeout used to wait for a server reply when the 'exec' method is used. Defaults to 2000 (ms).
  Duration execTimeout;

  /// A timeout used to wait for a server reply when the 'send' method is used. Defaults to 2000 (ms).
  Duration sendTimeout;

  /// Maximum buffer length in bytes which can be filled with response data. Defaults to 1M.
  int maxBufferLength;

  /// Enable/disable debug logs on console. Defaults to false.
  bool debug;

  Telnet({
    String host = '127.0.0.1',
    int port = 23,
    String localAddress = '',
    String shellPrompt = '/(?:/ )?#\s/',
    String loginPrompt = '/[lL]ogin[: ]*\$/i',
    String passwordPrompt = '/Password: /i',
    String failedLoginMatch,
    bool initialLFCR = false,
    String username = 'root',
    String password = '',
    String sock,
    String irs = '\r\n',
    String ors = '\n',
    int echoLines = 1,
    bool stripShellPrompt = true,
    String pageSeparator = '---- More',
    bool negotiationMandatory = true,
    Duration execTimeout,
    Duration sendTimeout,
    int maxBufferLength = 1048576,
    bool debug = false,
  }) {
    this.host = host;
    this.port = port;
    this.localAddress = localAddress;
    this.shellPrompt = shellPrompt;
    this.loginPrompt = loginPrompt;
    this.passwordPrompt = passwordPrompt;
    this.failedLoginMatch = failedLoginMatch;
    this.initialLFCR = initialLFCR;
    this.username = username;
    this.password = password;
    this.irs = irs;
    this.ors = ors;
    this.echoLines = echoLines;
    this.stripShellPrompt = stripShellPrompt;
    this.pageSeparator = pageSeparator;
    this.negotiationMandatory = negotiationMandatory;
    this.execTimeout = execTimeout == null ? Duration(seconds: 2) : execTimeout;
    this.sendTimeout = sendTimeout == null ? Duration(seconds: 2) : sendTimeout;
    this.maxBufferLength = maxBufferLength;
    this.debug = debug;
  }

  connect() async {
    var completer = Completer();
    _socket = await Socket.connect(host, port);

    // TODO provide way to connect to an existing socket, i.e. upgrading

    _state = 'start';
    this.fire(ConnectEvent());

    if (initialLFCR) _socket.write('\r\n');
    if (!negotiationMandatory) completer.complete();

    _inputBuffer = '';

    // TBD initialize timeout sequence on socket

    _socket.listen((Uint8List data) {
      if (_state == 'standby') {
        return this.fire(DataEvent(data));
      }
      _parseData(data, (event, parsed) {
        if (!completer.isCompleted && event == 'ready') {
          completer.complete(parsed);
        }
      });
    }, onError: (error) {
      this.fire(ErrorEvent(error));
      if (!completer.isCompleted) completer.completeError(error);
    }, onDone: () {
      this.fire(CloseEvent());
      if (!completer.isCompleted)
        completer.completeError(Future.error('Socket closes'));
    });

    return completer.future;
  }

  /// TBD ?
  /*_negotiate(Uint8List chunk) {
    /* info: http://tools.ietf.org/html/rfc1143#section-7
     * refuse to start performing and ack the start of performance
     * DO -> WONT WILL -> DO */
    var packetLength = chunk.length;

    var negData = chunk;
    var cmdData = null;
    var negResp = null;

    for (var i = 0; i < packetLength; i += 3) {
      if (chunk[i] != 255) {
        negData = chunk.sublist(0, i);
        cmdData = chunk.sublist(i);
        break;
      }
    }

    // negResp = negData.toString('hex').replace(/fd/g, 'fc').replace(/fb/g, 'fd')

    // if (this.socket.writable) this.socket.write(Buffer.from(negResp, 'hex'))

    // if (cmdData != undefined) return cmdData
    // else return
  }*/

  void _parseData(Uint8List chunk, callback) {
    if (chunk[0] == 255 && chunk[1] != 255) {
      // this.inputBuffer = ''
      // const negReturn = this._negotiate(chunk)

      // if (negReturn == undefined) return
      // else chunk = negReturn
      throw new Error();
    }

    if (_state == 'start') {
      _state = 'getprompt';
    }

    if (_state == 'getprompt') {
      var stringData = chunk.map((d) => String.fromCharCode(d)).join('');

      var promptIndex = search(stringData, shellPrompt);

      if (search(stringData, loginPrompt) != -1) {
        // guard from infinite loop
        if (!loginPromptReceived) {
          _state = 'login';
          _login('username');
          loginPromptReceived = true;
        }
      } else if (search(stringData, passwordPrompt) != -1) {
        _state = 'login';
        _login('password');
      } else if (failedLoginMatch != null &&
          search(stringData, failedLoginMatch) != -1) {
        _state = 'failedLogin';
        this.fire(FailedLoginEvent());
        destroy();
      } else if (promptIndex != -1) {
        if (!(shellPrompt is RegExp))
          shellPrompt = stringData.substring(promptIndex);

        _state = 'standby';
        _inputBuffer = '';
        loginPromptReceived = false;

        this.fire(ReadyEvent(shellPrompt));
        if (callback != null) callback('ready', shellPrompt);
      } else
        return;
    } else if (_state == 'response') {
      if (_inputBuffer.length >= maxBufferLength) {
        this.fire(BufferexceededEvent());
        throw new Error();
      }

      var stringData = chunk.map((d) => String.fromCharCode(d)).join('');

      _inputBuffer += stringData;
      var promptIndex = search(_inputBuffer, shellPrompt);

      if (promptIndex == -1 && stringData.length != 0) {
        if (search(stringData, pageSeparator) != -1) {
          _socket.write(new String.fromCharCode(20));
        }
        return;
      }

      _response = _inputBuffer.split(irs);
      promptSameLine = false;

      for (var i = 0; i < _response.length; i++) {
        if (search(_response[i], pageSeparator) != -1) {
          _response[i] = _response[i].replaceFirst(pageSeparator, '');

          if (_response[i].length == 0) _response.removeAt(i);
        } else if (_response[i].indexOf(shellPrompt) != -1) {
          promptSameLine = true;
        }
      }

      if (echoLines == 1)
        _response.removeAt(0);
      else if (echoLines > 1) _response.removeRange(0, echoLines);

      /* remove prompt */
      if (stripShellPrompt) {
        if (promptSameLine) {
          _response[_response.length - 1] =
              _response[_response.length - 1].replaceFirst(shellPrompt, '');
        } else {
          _response.removeLast();
          _response.add('');
        }
      }

      this.fire(ResponsereadyEvent());
    }
  }

  destroy() {
    throw new Error(); // TBD
  }

  _login(String handle) {
    if (handle == 'username' && !_isSocketClosed) {
      _socket.write(username);
      _state = 'getprompt';
    } else if (handle == 'password' && !_isSocketClosed) {
      _socket.write(password);
      _state = 'getprompt';
    }
  }

  Future<String> exec(String cmd) async {
    var completer = Completer<String>();

    cmd += ors;

    // TODO check if we need this at all
    /* if(_socket.writeable)
      throw new Error() */

    await _socket.write(cmd);
    var execTimeout = null;
    _state = 'response';
    this.fire(WritedoneEvent());
    bufExcHandler(BufferexceededEvent _) {
      // TODO type this
      // if(execTimeout != null) {
      // TODO
      // }
      if (_inputBuffer == null)
        return completer.completeError(Future.error('response not received'));

      completer.complete(_inputBuffer);

      _inputBuffer = '';
      _state = 'standyby';
    }

    StreamSubscription bufExceededSubscription;

    responseHandler(ResponsereadyEvent _) {
      // TODO type this
      // if(execTimeout != null) {
      // TODO
      // }
      if (_response != null) {
        completer.complete(_response.join('\n'));
      } else {
        completer.completeError(Future.error('invalid response'));
      }
      _inputBuffer = '';

      /* set state back to 'standby' for possible telnet server push data */
      _state = 'standyby';

      bufExceededSubscription.cancel();
    }

    bufExceededSubscription = once<BufferexceededEvent>(this, bufExcHandler);
    once<ResponsereadyEvent>(this, responseHandler);
    // if(this.execTimeout != null) {
    // TODO timeout
    // }

    return completer.future;
  }

  send(String data, {String waitfor}) async {
    var completer = Completer();

    data += ors;

    await _socket.write(data);
    var response = '';
    _state = 'standby';
    StreamSubscription subscription;
    if (waitfor == null)
      Timer(sendTimeout, () {
        subscription.cancel();
        if (response == '') {
          completer.completeError(Future.error('response not recieved'));
        } else {
          completer.complete(response);
        }
      });
    subscription = this.on<DataEvent>().listen((data) {
      response += data.data;
      if (waitfor != null && !response.contains(waitfor))
        subscription.cancel();
      else
        completer.complete(response);
    });

    return completer.future;
  }

  disconnect() {
    // TODO check if more implementation is needed here
    _socket.destroy();
  }
}
