import 'dart:async';
import 'dart:typed_data';

import 'package:event_bus/event_bus.dart';

int search(String str, dynamic pattern) {
  if (pattern is RegExp)
    throw new Error();
  else
    return str.indexOf(pattern);
}

/// utility fn for EventBus: run event listener only once.
StreamSubscription once<T>(EventBus bus, void Function(T data) callback) {
  var stream = bus.on<T>();
  StreamSubscription<T> subscription;
  subscription = stream.listen((data) {
    callback(data);
    subscription.cancel();
  });
  return subscription;
}
