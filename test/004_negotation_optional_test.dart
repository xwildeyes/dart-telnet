import 'dart:io';
import 'package:telnet/test.dart' as prefix0;
import "package:test/test.dart";
import "package:telnet/main.dart";

void main() {
  Process serverProcess;

  setUp(() => prefix0
      .setupNodeTelnetServer('004_negotation_optional')
      .then((process) => serverProcess = process));

  tearDown(() async {
    serverProcess.kill(ProcessSignal.sigkill);
  });

  test("send_data_without_options", () async {
    var con = Telnet(
        host: prefix0.host,
        port: prefix0.port,
        negotiationMandatory: false,
        sendTimeout: Duration(milliseconds: 100));

    await con.connect();
    var res = await con.send('Hello, server.');
    await con.disconnect();
    expect(res, equals('Hello, user.\r\n'));
  });

  //TBD testcase for exec_regex_shellprompt
}
