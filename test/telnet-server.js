/**
 * Telnet server in nodejs to support the tests
 */
let debug = require('debug')
debug.enable('node-telnet-server')
debug = debug('node-telnet-server')
const testName = process.argv[2]

// we preserve the original names of the tests for easy copy paste until first release.
const telnet_server = {
  createServer: (callback) => require('telnet').createServer(req => {
    req.on('data', data => debug('telnet received:', data.toString()))
    const write = req.write.bind(req)
    req.write = (buf) => {
      debug('telnet writing:', buf.toString())
      write(buf)
    }
    callback(req)
  })
}

const callback = () => {
  // this is a console.log instead of debug because the setUp hook relies on it
  // for initialization of tests.
  console.log('Telnet server listening on 2323...')
}

switch (testName) {
  case '002_busybox': {
    srv = telnet_server.createServer(function (c) {
      c.write(new Buffer("BusyBox v1.19.2 () built-in shell (ash)\n"
        + "Enter 'help' for a list of built-in commands.\n\n/ # ", 'ascii'))

      c.on('data', function () {
        c.write(new Buffer("uptime\r\n23:14  up 1 day, 21:50, 6 users, "
          + "load averages: 1.41 1.43 1.41\r\n", 'ascii'))
        c.write(new Buffer("/ # ", 'ascii'))
      })
    })

    srv.listen(2323, function () {
      callback()
    })
  } break
  case '003_server_push': {
    srv = telnet_server.createServer(function (c) {
      c.write(new Buffer("BusyBox v1.19.2 () built-in shell (ash)\n"
        + "Enter 'help' for a list of built-in commands.\n\n/ # ", 'ascii'))

      setTimeout(function () {
        c.write(new Buffer("Hello, client!", 'ascii'))
      }, 500)
    })

    srv.listen(2323, function () {
      callback()
    })
  } break
  case '004_negotation_optional': {
    srv = telnet_server.createServer(function (c) {
      c.on('data', function () {
        c.write(new Buffer("Hello, user.\n"))
      })
    })

    srv.listen(2323, function () {
      callback()
    })
  } break
  default: {
    debug('No test case was selected')
    process.exit(1)
  }
}
debug(`Setting up ${testName} in node..`)