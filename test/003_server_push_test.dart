import 'dart:async';
import 'dart:convert';
import 'dart:io';
import 'package:telnet/events.dart';
import 'package:telnet/test.dart' as prefix0;
import "package:test/test.dart";
import "package:telnet/main.dart";

void main() {
  Process serverProcess;

  setUp(() => prefix0
      .setupNodeTelnetServer('003_server_push')
      .then((process) => serverProcess = process));

  tearDown(() async {
    serverProcess.kill(ProcessSignal.sigkill);
  });

  tearDown(() async {
    serverProcess.kill(ProcessSignal.sigkill);
  });

  test("server_push", () async {
    var con = Telnet(
      host: prefix0.host,
      port: prefix0.port,
      shellPrompt: '/ # ',
      // timeout: 1500, // TBD
    );

    await con.connect();

    con.on<DataEvent>().listen((data) async {
      print(data);

      expect(data, equals('Hello, client!'));
      await con.disconnect();
    });
  });
}
