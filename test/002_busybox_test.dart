import 'dart:io';
import 'package:telnet/test.dart' as prefix0;
import "package:test/test.dart";
import "package:telnet/main.dart";

void main() {
  Process serverProcess;

  setUp(() => prefix0
      .setupNodeTelnetServer('002_busybox')
      .then((process) => serverProcess = process));

  tearDown(() async {
    serverProcess.kill(ProcessSignal.sigkill);
  });

  test("exec_string_shellprompt", () async {
    var con = Telnet(
      host: prefix0.host,
      port: prefix0.port,
      shellPrompt: '/ # ',
      // timeout: 1500, // TBD
    );

    await con.connect();
    var res = await con.exec('uptime');
    await con.disconnect();
    expect(
        res,
        equals(
            '23:14  up 1 day, 21:50, 6 users, load averages: 1.41 1.43 1.41\n'));
  });

  //TBD testcase for exec_regex_shellprompt
}
