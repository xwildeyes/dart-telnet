import 'dart:io';
import 'package:telnet/test.dart' as prefix0;
import "package:test/test.dart";
import "package:telnet/main.dart";

void main() {
  Process.run('ls', ['-l']).then((ProcessResult results) {
    print(results.stdout);
  });
  ServerSocket server;
  Telnet socket;
  int callbackCount;

  setUp(() async {
    socket = Telnet(
      host: prefix0.host,
      port: prefix0.port,
    );
    callbackCount = 0;

    server = await ServerSocket.bind('127.0.0.1', 2323);

    server.listen((socket) => callbackCount++);
  });

  tearDown(() async {
    await server.close();
  });

  test("Connects", () async {
    await socket.connect();

    expect(callbackCount, equals(1));
  });
}
